const Benchmark = require('benchmark');

const cepjs = require('cepjs');
const cepjsMost = require('cepjs-most');
const cepjsMostCore = require('cepjs-mostcore');

const cepjsOp = require('cepjs/operators');
const cepjsMostOp = require('cepjs-most/operators');
const cepjsMostCoreOp = require('cepjs-mostcore/operators');

function valueMinBench(benchOptions, logger, setFinished){
    let suite = new Benchmark.Suite;

    const benchOpts = Object.assign({defer: true}, benchOptions);
    const optInterval = 500;
    const optTake = 50;
    const optTumbWindow = 10;

    suite.add('valueMin cepjs',
        function(deferred){
            cepjs.interval(optInterval)
                .pipe(cepjsOp.take(optTake), cepjsOp.tumblingCountWindow(optTumbWindow),
                    cepjsOp.valueMin(['interval event'], 'payload', min => min <= 5, 'valueMin event'))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).add('valueMin cepjs-most',
        function(deferred){
            cepjsMost.interval(optInterval)
                .pipe(cepjsMostOp.take(optTake), cepjsMostOp.tumblingCountWindow(optTumbWindow),
                    cepjsMostOp.valueMin(['interval event'], 'payload', min => min <= 5, 'valueMin event'))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).add('valueMin cepjs-mostcore',
        function(deferred){
            cepjsMostCore.interval(optInterval)
                .pipe(cepjsMostCoreOp.take(optTake), cepjsMostCoreOp.tumblingCountWindow(optTumbWindow),
                    cepjsMostCoreOp.valueMin(['interval event'], 'payload', min => min <= 5, 'valueMin event'))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).on('cycle', function(event) {
        logger(event.target.toString());
    })
    .on('complete', function() {
        logger('Fastest is ' + this.filter('fastest').map('name').join(', '));
        setFinished();
    })
    .run({'async': true});
};

module.exports = valueMinBench;