const countBench = require('./count');
const valueMaxBench = require('./valuemax');
const valueMinBench = require('./valuemin');
const valueAvgBench = require('./valueavg');

module.exports = {
    countBench,
    valueMaxBench,
    valueMinBench,
    valueAvgBench
};