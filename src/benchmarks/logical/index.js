const allBench = require('./all');
const anyBench = require('./any');
const absenceBench = require('./absence');

module.exports = {
    allBench,
    anyBench,
    absenceBench
};