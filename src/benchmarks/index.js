const gp = require('./general-purpose');
const cont = require('./context');
const log = require('./logical');
const ts = require('./threshold');
const sub = require('./subset');
const mod = require('./modal');
const td = require('./trend');
const trf = require('./transformation');
const spt = require('./spatial');

module.exports = {
    tap: gp.tapBench,
    retry: gp.retryBench,
    filter: gp.filterBench,
    merge: gp.mergeBench,
    map: gp.mapBench,
    throttle: gp.throttleBench,
    skip: gp.skipBench,
    skipWhile: gp.skipWhileBench,
    skipUntil: gp.skipUntilBench,
    take: gp.takeBench,
    takeWhile: gp.takeWhileBench,
    takeUntil: gp.takeUntilBench,
    flatMap: gp.flatMapBench,
    mergeMap: gp.flatMapBench,
    multicast: gp.multicastBench,
    share: gp.multicastBench,
    delay: gp.delayBench,
    concatMap: gp.concatMapBench,
    //context
    tumblingCountWindow: cont.tumbCountBench,
    tumblingTimeWindow: cont.tumbTimeBench,
    slidingCountWindow: cont.slidCountBench,
    slidingTimeWindow: cont.slidTimeBench,
    eventIntervalWindow: cont.evtIntervalBench,
    fixedIntervalWindow: cont.fixedIntervalBench,
    //logical
    all: log.allBench,
    any: log.anyBench,
    absence: log.absenceBench,
    //threshold
    count: ts.countBench,
    valueMax: ts.valueMaxBench,
    valueMin: ts.valueMinBench,
    valueAvg: ts.valueAvgBench,
    //subset
    nHighestValues: sub.nHighBench,
    nLowestValues: sub.nLowBench,
    //modal
    always: mod.alwaysBench,
    sometimes: mod.someBench,
    //trend
    increasing: td.incBench,
    decreasing: td.decBench,
    stable: td.stabBench,
    nonIncreasing: td.nonIncBench,
    nonDecreasing: td.nonDecBench,
    mixed: td.mixBench,
    //transformation
    project: trf.proBench,
    //spatial
    minDistance: spt.minDistBench,
    maxDistance: spt.maxDistBench,
    avgDistance: spt.avgDistBench,
    relativeMinDistance: spt.relatMinDistBench,
    relativeMaxDistance: spt.relatMaxDistBench,
    relativeAvgDistance: spt.relatAvgDistBench
};