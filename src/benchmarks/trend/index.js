const incBench = require('./increasing');
const decBench = require('./decreasing');
const mixBench = require('./mixed');
const nonIncBench = require('./nonIncreasing');
const nonDecBench = require('./nonDecreasing');
const stabBench = require('./stable');

module.exports = {
    incBench,
    decBench,
    mixBench,
    nonIncBench,
    nonDecBench,
    stabBench
}