const Benchmark = require('benchmark');

const cepjs = require('cepjs');
const cepjsMost = require('cepjs-most');
const cepjsMostCore = require('cepjs-mostcore');

const cepjsOp = require('cepjs/operators');
const cepjsMostOp = require('cepjs-most/operators');
const cepjsMostCoreOp = require('cepjs-mostcore/operators');

const { Point } = cepjs;

const generateRandomPoint = require('./randompoint');

function minDistBench(benchOptions, logger, setFinished){
    let suite = new Benchmark.Suite;

    const benchOpts = Object.assign({defer: true}, benchOptions);
    const optInterval = 500;
    const optTake = 50;
    const optTumbWindow = 10;
    const fixedPoint = new Point(46.01001, -86.3195);

    suite.add('minDistance cepjs',
        function(deferred){
            cepjs.interval(optInterval)
                .pipe(cepjsOp.take(optTake), cepjsOp.mergeMap(x => cepjs.of(generateRandomPoint())),
                    cepjsOp.tumblingCountWindow(optTumbWindow),
                    cepjsOp.minDistance(['of event'], fixedPoint,
                                'payload', distance => distance > 500, 'minDistance event'))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).add('minDistance cepjs-most',
        function(deferred){
            cepjsMost.interval(optInterval)
                .pipe(cepjsMostOp.take(optTake), cepjsMostOp.mergeMap(x => cepjsMost.of(generateRandomPoint())),
                    cepjsMostOp.tumblingCountWindow(optTumbWindow),
                    cepjsMostOp.minDistance(['of event'], fixedPoint,
                                'payload', distance => distance > 500, 'minDistance event'))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).add('minDistance cepjs-mostcore',
        function(deferred){
            cepjsMostCore.interval(optInterval)
                .pipe(cepjsMostCoreOp.take(optTake),
                    cepjsMostCoreOp.mergeMap(x => cepjsMostCore.of(generateRandomPoint())),
                    cepjsMostCoreOp.tumblingCountWindow(optTumbWindow),
                    cepjsMostCoreOp.minDistance(['of event'], fixedPoint,
                                'payload', distance => distance > 500, 'minDistance event'))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).on('cycle', function(event) {
        logger(event.target.toString());
    })
    .on('complete', function() {
        logger('Fastest is ' + this.filter('fastest').map('name').join(', '));
        setFinished();
    })
    .run({'async': true});
};

module.exports = minDistBench;