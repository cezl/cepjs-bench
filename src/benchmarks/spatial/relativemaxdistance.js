const Benchmark = require('benchmark');

const cepjs = require('cepjs');
const cepjsMost = require('cepjs-most');
const cepjsMostCore = require('cepjs-mostcore');

const cepjsOp = require('cepjs/operators');
const cepjsMostOp = require('cepjs-most/operators');
const cepjsMostCoreOp = require('cepjs-mostcore/operators');

const generateRandomPoint = require('./randompoint');

function relatMaxDistBench(benchOptions, logger, setFinished){
    let suite = new Benchmark.Suite;
    
    const benchOpts = Object.assign({defer: true}, benchOptions);
    const optInterval = 500;
    const optTake = 50;
    const optTumbWindow = 10;

    suite.add('relativeMaxDistance cepjs',
        function(deferred){
            cepjs.interval(optInterval)
                .pipe(cepjsOp.take(optTake), cepjsOp.mergeMap(x => cepjs.of(generateRandomPoint())),
                    cepjsOp.tumblingCountWindow(optTumbWindow),
                    cepjsOp.relativeMaxDistance(['of event'],
                                'payload', distance => distance > 500, 'relativeMaxDistance event'))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).add('relativeMaxDistance cepjs-most',
        function(deferred){
            cepjsMost.interval(optInterval)
                .pipe(cepjsMostOp.take(optTake), cepjsMostOp.mergeMap(x => cepjsMost.of(generateRandomPoint())),
                    cepjsMostOp.tumblingCountWindow(optTumbWindow),
                    cepjsMostOp.relativeMaxDistance(['of event'],
                                'payload', distance => distance > 500, 'relativeMaxDistance event'))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).add('relativeMaxDistance cepjs-mostcore',
        function(deferred){
            cepjsMostCore.interval(optInterval)
                .pipe(cepjsMostCoreOp.take(optTake),
                    cepjsMostCoreOp.mergeMap(x => cepjsMostCore.of(generateRandomPoint())),
                    cepjsMostCoreOp.tumblingCountWindow(optTumbWindow),
                    cepjsMostCoreOp.relativeMaxDistance(['of event'],
                                'payload', distance => distance > 500, 'relativeMaxDistance event'))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).on('cycle', function(event) {
        logger(event.target.toString());
    })
    .on('complete', function() {
        logger('Fastest is ' + this.filter('fastest').map('name').join(', '));
        setFinished();
    })
    .run({'async': true});
};

module.exports = relatMaxDistBench;