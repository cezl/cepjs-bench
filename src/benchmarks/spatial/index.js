const minDistBench = require('./mindistance');
const maxDistBench = require('./maxdistance');
const avgDistBench = require('./avgdistance');
const relatMinDistBench = require('./relativemindistance');
const relatMaxDistBench = require('./relativemaxdistance');
const relatAvgDistBench = require('./relativeavgdistance');

module.exports = {
    minDistBench,
    maxDistBench,
    avgDistBench,
    relatMinDistBench,
    relatMaxDistBench,
    relatAvgDistBench
};