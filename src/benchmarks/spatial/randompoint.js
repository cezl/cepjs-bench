const { Point } = require('cepjs');
const randomCoordinates = require('random-coordinates');

function generateRandomPoint(){
    let [latitude, longitude] = randomCoordinates().split(', ').map(coord => parseFloat(coord));
    return new Point(latitude, longitude);
}

module.exports = generateRandomPoint;