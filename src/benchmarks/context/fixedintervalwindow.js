const Benchmark = require('benchmark');

const cepjs = require('cepjs');
const cepjsMost = require('cepjs-most');
const cepjsMostCore = require('cepjs-mostcore');

const cepjsOp = require('cepjs/operators');
const cepjsMostOp = require('cepjs-most/operators');
const cepjsMostCoreOp = require('cepjs-mostcore/operators');

function fixedIntervalBench(benchOptions, logger, setFinished){
    let suite = new Benchmark.Suite;

    const benchOpts = Object.assign({defer: true}, benchOptions);
    const optInterval = 500
    const optTake = 120;
    const optStartDelay = 2000;
    const optEndTime = 2000;

    suite.add('fixedIntervalWindow cepjs',
        function(deferred){
            cepjs.interval(optInterval)
                .pipe(cepjsOp.take(optTake),
                cepjsOp.fixedIntervalWindow(new Date(new Date().getTime() + optStartDelay), optEndTime,
                cepjs.recurrence.NONE, cepjs.ordering.OCCURRENCE_TIME))
                .subscribe({
                    next: (value) =>{},
                    error: (error) =>{},
                    complete: () =>{
                        deferred.resolve();
                    }
                });
        }, benchOpts
    ).add('fixedIntervalWindow cepjs-most',
        function(deferred){
            cepjsMost.interval(optInterval)
                .pipe(cepjsMostOp.take(optTake),
                cepjsMostOp.fixedIntervalWindow(new Date(new Date().getTime() + optStartDelay), optEndTime,
                cepjsMost.recurrence.NONE, cepjsMost.ordering.OCCURRENCE_TIME))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).add('fixedIntervalWindow cepjs-mostcore',
        function(deferred){
            cepjsMostCore.interval(optInterval)
                .pipe(cepjsMostCoreOp.take(optTake),
                    cepjsMostCoreOp.fixedIntervalWindow(new Date(new Date().getTime() + optStartDelay), optEndTime,
                    cepjsMostCore.recurrence.NONE, cepjsMostCore.ordering.OCCURRENCE_TIME))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).on('cycle', function(event) {
        logger(event.target.toString());
    })
    .on('complete', function() {
        logger('Fastest is ' + this.filter('fastest').map('name').join(', '));
        setFinished();
    })
    .run({'async': true});
};

module.exports = fixedIntervalBench;