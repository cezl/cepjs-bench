const tumbCountBench = require('./tumblingcountwindow');
const slidCountBench = require('./slidingcountwindow');
const tumbTimeBench = require('./tumblingtimewindow');
const slidTimeBench = require('./slidingtimewindow');
const evtIntervalBench = require('./eventintervalwindow');
const fixedIntervalBench = require('./fixedintervalwindow');
const groupByBench = require('./groupby');

module.exports = {
    tumbCountBench, tumbTimeBench,
    slidCountBench, slidTimeBench,
    evtIntervalBench,
    fixedIntervalBench,
    groupByBench
};