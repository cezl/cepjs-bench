const Benchmark = require('benchmark');

const cepjs = require('cepjs');
const cepjsMost = require('cepjs-most');
const cepjsMostCore = require('cepjs-mostcore');

const cepjsOp = require('cepjs/operators');
const cepjsMostOp = require('cepjs-most/operators');
const cepjsMostCoreOp = require('cepjs-mostcore/operators');

function skipBench(benchOptions, logger, setFinished){
    let suite = new Benchmark.Suite;

    const benchOpts = Object.assign({defer: true}, benchOptions);
    const optInterval = 500;
    const optSkip = 80;

    suite.add('skip cepjs',
        function(deferred){
            cepjs.interval(optInterval)
                .pipe(cepjsOp.skip(optSkip), cepjsOp.take(1))
                .subscribe({
                    next: (value) =>{},
                    error: (error) =>{},
                    complete: () =>{
                        deferred.resolve();
                    }
                });
        }, benchOpts
    ).add('skip cepjs-most',
        function(deferred){
            cepjsMost.interval(optInterval)
                .pipe(cepjsMostOp.skip(optSkip), cepjsMostOp.take(1))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).add('skip cepjs-mostcore',
        function(deferred){
            cepjsMostCore.interval(optInterval)
                .pipe(cepjsMostCoreOp.skip(optSkip), cepjsMostCoreOp.take(1))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).on('cycle', function(event) {
        logger(event.target.toString());
    })
    .on('complete', function() {
        logger('Fastest is ' + this.filter('fastest').map('name').join(', '));
        setFinished();
    })
    .run({'async': true});
};

module.exports = skipBench;