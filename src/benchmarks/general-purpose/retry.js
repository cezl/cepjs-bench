const Benchmark = require('benchmark');

const cepjs = require('cepjs');
const cepjsMost = require('cepjs-most');
const cepjsMostCore = require('cepjs-mostcore');

const cepjsOp = require('cepjs/operators');
const cepjsMostOp = require('cepjs-most/operators');
const cepjsMostCoreOp = require('cepjs-mostcore/operators');

function retryBench(benchOptions, logger, setFinished){
    let suite = new Benchmark.Suite;
    
    const optThreshold = 70;
    const optInterval = 500;
    const optRetry = 3;
    const benchOpts = Object.assign({defer: true}, benchOptions);

    let filterPayload = evt =>{
        if(evt.payload == optThreshold){
            throw new Error('found the number');
        }
          return evt;
    };

    suite.add('retry cepjs',
        function(deferred){
            cepjs.interval(optInterval).pipe(cepjsOp.map(filterPayload),
                cepjsOp.retry(optRetry)).subscribe({
                    next: (value) =>{},
                    error: (error) =>{},
                    complete: () =>{
                        deferred.resolve();
                    }
                });
        }, benchOpts
    ).add('retry cepjs-most',
        function(deferred){
            cepjsMost.interval(optInterval)
            .pipe(cepjsMostOp.map(filterPayload),
              cepjsMostOp.retry(optRetry)).subscribe({
                next: (value) =>{},
                error: (error) =>{},
                complete: () =>{
                    deferred.resolve();
                }
            });
        }, benchOpts
    ).add('retry cepjs-mostcore',
        function(deferred){
            cepjsMostCore.interval(optInterval)
            .pipe(cepjsMostCoreOp.map(filterPayload),
              cepjsMostCoreOp.retry(optRetry)).subscribe({
                next: (value) =>{},
                error: (error) =>{},
                complete: () =>{
                    deferred.resolve();
                }
            });
        }, benchOpts
    ).on('cycle', function(event) {
        logger(event.target.toString());
    })
    .on('complete', function() {
        logger('Fastest is ' + this.filter('fastest').map('name').join(', '));
        setFinished();
    })
    .run({'async': true});
}

module.exports = retryBench;