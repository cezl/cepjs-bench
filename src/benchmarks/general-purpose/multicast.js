const Benchmark = require('benchmark');

const cepjs = require('cepjs');
const cepjsMost = require('cepjs-most');
const cepjsMostCore = require('cepjs-mostcore');

const cepjsOp = require('cepjs/operators');
const cepjsMostOp = require('cepjs-most/operators');
const cepjsMostCoreOp = require('cepjs-mostcore/operators');

function multicastBench(benchOptions, logger, setFinished){
    let suite = new Benchmark.Suite;

    const optInterval = 500;
    const optTake = 80;
    const benchOpts = Object.assign({defer: true}, benchOptions);

    suite.add('multicast/share cepjs',
        function(deferred){
            let stream = cepjs.interval(optInterval)
                        .pipe(cepjsOp.take(optTake), cepjsOp.multicast());

            stream.pipe(cepjsOp.merge(stream))
                .subscribe({
                    next: (value) =>{},
                    error: (error) =>{},
                    complete: () =>{
                        deferred.resolve();
                    }
                });
        }, benchOpts
    ).add('multicast/share cepjs-most',
        function(deferred){
            let stream = cepjsMost.interval(optInterval)
                        .pipe(cepjsMostOp.take(optTake), cepjsMostOp.multicast());

            stream.pipe(cepjsMostOp.merge(stream))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).add('multicast/share cepjs-mostcore',
        function(deferred){
            let stream = cepjsMostCore.interval(optInterval)
                        .pipe(cepjsMostCoreOp.take(optTake), cepjsMostCoreOp.multicast());

            stream.pipe(cepjsMostCoreOp.merge(stream))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).on('cycle', function(event) {
        logger(event.target.toString());
    })
    .on('complete', function() {
        logger('Fastest is ' + this.filter('fastest').map('name').join(', '));
        setFinished();
    })
    .run({'async': true});
}

module.exports = multicastBench;