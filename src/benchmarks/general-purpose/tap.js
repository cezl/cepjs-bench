const Benchmark = require('benchmark');

const cepjs = require('cepjs');
const cepjsMost = require('cepjs-most');
const cepjsMostCore = require('cepjs-mostcore');

const cepjsOp = require('cepjs/operators');
const cepjsMostOp = require('cepjs-most/operators');
const cepjsMostCoreOp = require('cepjs-mostcore/operators');

function tapBench(benchOptions, logger, setFinished) {
    let suite = new Benchmark.Suite;

    const benchOpts = Object.assign({defer: true}, benchOptions);
    const optInterval = 500;
    const optTake = 80;

    suite.add('tap cepjs',
        function(deferred){
            cepjs.interval(optInterval)
                .pipe(cepjsOp.take(optTake), cepjsOp.tap(f => f))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).add('tap cepjs-most',
        function(deferred){
            cepjsMost.interval(optInterval)
                .pipe(cepjsMostOp.take(optTake), cepjsMostOp.tap(f => f))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).add('tap cepjs-mostcore',
        function(deferred){
            cepjsMostCore.interval(optInterval)
                .pipe(cepjsMostCoreOp.take(optTake), cepjsMostCoreOp.tap(f => f))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).on('cycle', function(event) {
        logger(event.target.toString());
    })
    .on('complete', function() {
        logger('Fastest is ' + this.filter('fastest').map('name').join(', '));
        setFinished();
    })
    .run({'async': true});
};

module.exports = tapBench;