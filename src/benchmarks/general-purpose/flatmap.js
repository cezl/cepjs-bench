const Benchmark = require('benchmark');

const cepjs = require('cepjs');
const cepjsMost = require('cepjs-most');
const cepjsMostCore = require('cepjs-mostcore');

const cepjsOp = require('cepjs/operators');
const cepjsMostOp = require('cepjs-most/operators');
const cepjsMostCoreOp = require('cepjs-mostcore/operators');

function flatMapBench(benchOptions, logger, setFinished){
    let suite = new Benchmark.Suite;

    const benchOpts = Object.assign({defer: true}, benchOptions);
    const optInterval1 = 500;
    const optInterval2 = 500;
    const optTake1 = 3;
    const optTake2 = 70;

    suite.add('flatMap/mergeMap cepjs',
        function(deferred){
            cepjs.interval(optInterval1)
                .pipe(cepjsOp.take(optTake1),
                    cepjsOp.flatMap(evt => cepjs.interval(optInterval2).pipe(cepjsOp.take(optTake2))))
                .subscribe({
                    next: (value) =>{},
                    error: (error) =>{},
                    complete: () =>{
                        deferred.resolve();
                    }
                });
        }, benchOpts
    ).add('flatMap/mergeMap cepjs-most',
        function(deferred){
            cepjsMost.interval(optInterval1)
                .pipe(cepjsMostOp.take(optTake1),
                    cepjsMostOp.flatMap(evt => cepjsMost.interval(optInterval2).pipe(cepjsMostOp.take(optTake2))))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).add('flatMap/mergeMap cepjs-mostcore',
        function(deferred){
            cepjsMostCore.interval(optInterval1)
                .pipe(cepjsMostCoreOp.take(optTake1),
                        cepjsMostCoreOp.flatMap(evt =>
                            cepjsMostCore.interval(optInterval2).pipe(cepjsMostCoreOp.take(optTake2))))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).on('cycle', function(event) {
        logger(event.target.toString());
    })
    .on('complete', function() {
        logger('Fastest is ' + this.filter('fastest').map('name').join(', '));
        setFinished();
    })
    .run({'async': true});
}

module.exports = flatMapBench;