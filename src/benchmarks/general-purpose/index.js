const tapBench = require('./tap');
const retryBench = require('./retry');
const filterBench = require('./filter');
const mergeBench = require('./merge');
const mapBench = require('./map');
const throttleBench = require('./throttle');
const skipBench = require('./skip');
const skipWhileBench = require('./skipwhile');
const skipUntilBench = require('./skipuntil');
const takeBench = require('./take');
const takeUntilBench = require('./takeuntil');
const takeWhileBench = require('./takewhile');
const flatMapBench = require('./flatmap');
const multicastBench = require('./multicast');
const delayBench = require('./delay');
const concatMapBench = require('./concatmap');

module.exports = {
    tapBench,
    retryBench,
    filterBench,
    mergeBench,
    mapBench,
    throttleBench,
    skipBench,
    skipWhileBench,
    skipUntilBench,
    takeBench,
    takeUntilBench,
    takeWhileBench,
    flatMapBench,
    multicastBench,
    delayBench,
    concatMapBench
};