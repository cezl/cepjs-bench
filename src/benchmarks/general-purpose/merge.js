const Benchmark = require('benchmark');

const cepjs = require('cepjs');
const cepjsMost = require('cepjs-most');
const cepjsMostCore = require('cepjs-mostcore');

const cepjsOp = require('cepjs/operators');
const cepjsMostOp = require('cepjs-most/operators');
const cepjsMostCoreOp = require('cepjs-mostcore/operators');

function mergeBench(benchOptions, logger, setFinished){
    let suite = new Benchmark.Suite;

    const benchOpts = Object.assign({defer: true}, benchOptions);
    const optInterval1 = 300;
    const optInterval2 = 500;
    const optTake = 80;

    suite.add('merge cepjs',
        function(deferred){
            cepjs.interval(optInterval1)
                .pipe(cepjsOp.merge(cepjs.interval(optInterval2)),
                        cepjsOp.take(optTake))
                .subscribe({
                    next: (value) =>{},
                    error: (error) =>{},
                    complete: () =>{
                        deferred.resolve();
                    }
                });
        }, benchOpts
    ).add('merge cepjs-most',
        function(deferred){
            cepjsMost.interval(optInterval1)
                .pipe(cepjsMostOp.merge(cepjsMost.interval(optInterval2)),
                        cepjsMostOp.take(optTake))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).add('merge cepjs-mostcore',
        function(deferred){
            cepjsMostCore.interval(optInterval1)
                .pipe(cepjsMostCoreOp.merge(cepjsMostCore.interval(optInterval2)), 
                        cepjsMostCoreOp.take(optTake))
                    .subscribe({
                        next: (value) =>{},
                        error: (error) =>{},
                        complete: () =>{
                            deferred.resolve();
                        }
                    });
        }, benchOpts
    ).on('cycle', function(event) {
        logger(event.target.toString());
    })
    .on('complete', function() {
        logger('Fastest is ' + this.filter('fastest').map('name').join(', '));
        setFinished();
    })
    .run({'async': true});
}

module.exports = mergeBench;