const nHighBench = require('./nhighestvalues');
const nLowBench = require('./nlowestvalues');

module.exports = {
    nHighBench,
    nLowBench
};