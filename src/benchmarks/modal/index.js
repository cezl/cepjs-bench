const alwaysBench = require('./always');
const someBench = require('./sometimes');

module.exports = {
    alwaysBench,
    someBench
};