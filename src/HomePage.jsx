import React from 'react';
import ReactDOM from 'react-dom';
import { Page, Toolbar, Select, Button, Card, ProgressCircular,
          List, ListItem, Checkbox, Input, Toast} from 'react-onsenui';

export default class HomePage extends React.Component {

  render() {
    
    return (
      <Page
        renderToolbar={() =>
          <Toolbar>
            <div className='center'>cepjs benchmark</div>
          </Toolbar>
        }
      >
        <Toast isOpen={this.props.toastMessage.length > 0} animation={"fall"}
          animationOptions={{duration: 1}} onPostHide={e => this.props.handleToast('')} />
        <Card style={{ textAlign: "center", marginTop: "30px"}}>
          <h3>Operation</h3>
          <Select value={this.props.selectedOp} onChange={this.props.handleOperationChange}
            disabled={this.props.runningBench}>
            {this.props.operations.map((val, index) => 
              <option value={val} key={index}>{val}</option>
            )}
          </Select>
          
          <h3 style={{marginTop: "20px"}}>Settings</h3>

          <List>
            {[...this.props.benchOpts.entries()].map((val, index)=>{
              let hashKey = val[0];
              let enabled = val[1].enabled;
              let value = val[1].value;

              return (
              <ListItem key={index}>
              <label className="left">
                <Checkbox input-id="check-2" checked={enabled}
                    onChange={e => this.props.handleEnabledChange(e, hashKey)}></Checkbox>
              </label>
              <Input id="data" value={value? value: "" } float modifier="underbar" placeholder={hashKey}
                disabled={!enabled} onChange={e => this.props.handleInputChange(e, hashKey)}
                ></Input>
              </ListItem>);
            }
            )}
          </List>

          <br />
          {this.props.runningBench &&
            <ProgressCircular indeterminate />}
          
          <br />
          <Button onClick={this.props.runBench}
            style={{marginTop: "50px"}} disabled={this.props.runningBench}>Benchmark</Button>

          <h3 style={{marginTop: "20px"}}>Output</h3>
          <fieldset style={{whiteSpace: "pre-wrap", height: "150px", overflow: "auto"}}>
            {this.props.output}
          </fieldset>
        </Card>
      </Page>
    );
  }
}