import React from 'react';
import ReactDOM from 'react-dom';
import {Tabbar, Tab} from 'react-onsenui';

import HomePage from './HomePage';

const operations = [
  //General-purpose stream manipulation
  "tap", "retry", "multicast", "share", "filter", "merge", "map", "throttle", "delay", "skip", "skipWhile", "skipUntil",
  "take", "takeWhile", "takeUntil", "mergeMap", "flatMap", "concatMap",
  //Context
  "tumblingCountWindow", "slidingCountWindow", "tumblingTimeWindow", "slidingTimeWindow", "fixedIntervalWindow", 
  "eventIntervalWindow", "groupBy",
  //Transformation
  "project",
  //Patterns
  //Logical
  "all", "any", "absence",
  //Modal
  "always", "sometimes",
  //Spatial
  "minDistance", "maxDistance", "avgDistance", "relativeMinDistance", "relativeMaxDistance", "relativeAvgDistance",
  //Subset
  "nHighestValues", "nLowestValues",
  //Threshold
  "count", "valueMax", "valueMin", "valueAvg",
  //Trend
  "increasing", "decreasing", "stable", "nonIncreasing", "nonDecreasing", "mixed"
  ];

const { Map } =  require('immutable');
const benchmark = require('./benchmarks');

export default class App extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      benchOpts: new Map([
        ["initCount", {enabled: false, value: undefined}],
        ["minTime", {enabled: false, value: undefined}],
        ["maxTime", {enabled: false, value: undefined}],
        ["minSamples", {enabled: false, value: undefined}]
      ]),
      runningBench: false,
      selectedOp: operations[0],
      output: "",
      toastMessage: ''
    };

    this.handleEnabledChange = this.handleEnabledChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleOperationChange = this.handleOperationChange.bind(this);
    this.runBench = this.runBench.bind(this);
    this.endBench = this.endBench.bind(this);
    this.log = this.log.bind(this);
    this.buildBenchOptions = this.buildBenchOptions.bind(this);
    this.handleToast = this.handleToast.bind(this);
  }

  handleEnabledChange(e, hashKey){
    this.setState(state => {
      let newEnabled = !state.benchOpts.get(hashKey).enabled;
      let newValue = !newEnabled ? undefined: state.benchOpts.get(hashKey).value;

      return ({benchOpts: state.benchOpts.set(hashKey, {enabled: newEnabled, value: newValue})});
    });
  }

  handleInputChange(e, hashKey){
    this.setState(state => {
      let enabled = state.benchOpts.get(hashKey).enabled;
      return {benchOpts: state.benchOpts.set(hashKey, {
        enabled: enabled,
        value: Number(e.target.value)
        })
      };
    });
  }

  handleOperationChange(e){
    this.setState({
      selectedOp: e.target.value
    });
  }

  log(newOutput){
    this.setState(state => {
      let currOutput = state.output;

      return ({
        output: (currOutput === '' ? newOutput : currOutput + "\n" + newOutput)
      })
    });
  }

  endBench(){
    this.setState({
      runningBench: false
    });
  }

  buildBenchOptions(){
    let benchOptions = {};
    [...this.state.benchOpts.entries()].forEach((val) =>{
      if(val[1].enabled){
        benchOptions[val[0]] = Number(val[1].value);
      }
    });
    
    return benchOptions;
  }

  runBench(){
    this.setState({
      runningBench: true,
      output: ""
    });

    let benchOptions = this.buildBenchOptions();

    try{
      benchmark[this.state.selectedOp](benchOptions, this.log, this.endBench);
    }catch(e){
      this.handleToast(e.message);
    }
  }

  handleToast(message){
    this.setState({
      toastMessage: message
    });
  }
  
  
  renderTabs() {
    return [
      {
        content: <HomePage operations={operations}
          runningBench={this.state.runningBench} runBench={this.runBench}
          handleOperationChange={this.handleOperationChange} output={this.state.output}
          selectedOp = {this.state.selectedOp} benchOpts={this.state.benchOpts} 
          handleEnabledChange={this.handleEnabledChange}
          handleInputChange={this.handleInputChange}
          handleToast={this.handleToast} toastMessage={this.state.toastMessage} key="homespage" />,
        tab: <Tab label='Home' icon='md-home' key="tab1" />
      }
    ]
  }

  render() {
    return (
      <Tabbar initialIndex={0} renderTabs={this.renderTabs.bind(this)} />
    );
  }
}